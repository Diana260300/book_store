<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail_buku extends Model
{
    use HasFactory;

    protected $table ="detail-bukus";

    public function detail_pesans()
    {
        return $this->hasMany(detail_pesan::class);
    }
}
