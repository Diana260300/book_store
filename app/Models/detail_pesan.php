<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detail_pesan extends Model
{
    use HasFactory;

    protected $table ="detail_pesans";

    public function detail_bukus()
    {
        return $this->belongsTo(Detail_buku::class);
    }

    public function pesanans()
    {
        return $this->belongsTo(pesanan::class);
    }
}
