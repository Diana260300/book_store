<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detailpesanan extends Model
{
    protected $table ="detailpesanans";
    protected $primarikey ="id";
    protected $fillable = [
        'id',
        'user_id', 
        'tanggal',
        'nama_buku',
        'stok_terjual',
        'total'
    ];
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
