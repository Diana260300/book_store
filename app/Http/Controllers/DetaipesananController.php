<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\detailpesanan;

class DetaipesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Pesanan = detailpesanan::all();
        return view('detailpesanan', compact('Pesanan'));
    }
    public function detail_buku()
    {
        $Pesanan = detailpesanan::all();
        return view('detail_buku', compact('Pesanan'));
    }
    public function detail_pesanan_buku()
    {
        $Pesanan = detailpesanan::all();
        return view('detail_pesanan_buku', compact('Pesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pesan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd ($request->all());
        detailpesanan::create([
            'id' => $request->id,
            'tanggal' => $request->tanggal,
            'nama_buku' => $request->nama_buku,
            'stok_terjual' => $request->stok_terjual,
            'total' => $request->total,
        ]);
        return redirect('detail_pesanan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = detailpesanan::find($id);
        return view('edit-data', compact('data'));
    }
    public function detpesanan($id)
    {
        $data = detailpesanan::find($id);
        return view('detail_pesanan', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = detailpesanan::find($id);
        $save = $data->update([
            'id' => $request->id,
            'tanggaal' => $request->tanggal,
            'nama_buku' => $request->nama_buku,
            'stok_terjual' => $request->stok_terjual,
            'total' => $request->total,
        ]);
        if($save){
            $Pesanan = detailpesanan::all();
            return redirect()->route('detail_pesanan', compact('Pesanan'));
        }
        
    }
    public function simpan_tambah(Request $request, $id)
    {
        $data = detailpesanan::find($id);
        $save = $data->update([
            'id' => $request->id,
            'tanggaal' => $request->tanggal,
            'nama_buku' => $request->nama_buku,
            'stok_terjual' => $request->stok_terjual,
            'total' => $request->total,
        ]);
        if($save){
            $Pesanan = detailpesanan::all();
            return redirect()->route('detail_pesanan_buku', compact('Pesanan'));
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = detailpesanan::findorfail($id);
        $pes->delete();
        return back();
    }
}
