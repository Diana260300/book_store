<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPesansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pesans', function (Blueprint $table) {
            $table->id();
            //$table->integer ('detail_buku_id')->unsigned();
            //$table->integer ('pesanan_id')->unsigned();
            $table->string ('judul_buku')-> length (100);
            $table->integer ('harga')-> length (50);
            $table->integer ('jumlah')-> length (10);
            $table->integer ('jumlah_harga')-> length (10);
            $table->timestamps();

            //$table->foreign('detail_buku_id')->references('id')->on('detail_bukus')->onDelete('cascade');
            //$table->foreign('pesanan_id')->references('id')->on('pesanans')->onDelete('cascade');

            //$table->foreign('detail_buku_id')->references('id')->on('detail_buku')->onDelete('cascade');
            //$table->foreign('pesanan_id')->references('id')->on('pesanan')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pesans');
    }
}
