<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\BukuController;

use App\Http\Controllers\TambahbukuController;


use App\Http\Controllers\DetaipesananController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route:: get('login', [LoginController::class, 'index'])  -> name ('login') -> middleware('guest');
Route:: post('login', [LoginController::class, 'authenticate']);
Route:: post('logout', [LoginController::class, 'logout']);

Route:: get('register', [RegisterController::class, 'index'])-> middleware('guest');
Route:: post('register', [RegisterController::class, 'store']);

Route:: get ('dashboard', [Dashboard::class, 'index']) -> middleware('auth');
Route:: get('dashboard/buku', [BukuController:: class, 'index']) -> middleware('isAdmin');



Route:: get('tampil', [TambahbukuController::class, 'index']);
Route:: get('tambah', [TambahbukuController::class, 'create']);
Route:: get('simpan', [TambahbukuController::class, 'store']);

Route::get('pesanan', function () {
    return view('tambah-data');
});

Route::get('/detail_pesanan', [DetaipesananController::class, 'index'])->name('detail_pesanan') ;    
Route::get('/detail_buku', [DetaipesananController::class, 'detail_buku'])->name('detail_buku') ;    
Route::get('/detail_pesanan_buku', [DetaipesananController::class, 'detail_pesanan_buku'])->name('detail_pesanan_buku') ;    
Route::get('/detailpesanan', [DetaipesananController::class, 'detailpesanan'])->name('detailpesanan') ;    
Route::get('/tambahpesan', [DetaipesananController::class, 'tambahpesanan'])->name('tambahpesan') ;    
Route::post('/simpan', [DetaipesananController::class, 'store'])->name('simpan') ;    
Route::get('/edit/{id}', [DetaipesananController::class, 'edit'])->name('edit') ;    
Route::get('/detpesanan/{id}', [DetaipesananController::class, 'detpesanan'])->name('detpesanan') ;    
Route::put('/edit/{id}', [DetaipesananController::class, 'update'])->name('edit') ;    
Route::put('/simpan_tambah/{id}', [DetaipesananController::class, 'simpan_tambah'])->name('simpan_tambah') ;    
Route::get('/hapus/{id}', [DetaipesananController::class, 'destroy'])->name('hapus') ; 

