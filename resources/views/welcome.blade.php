<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>BookStore</title>
  </head>
  <body>
    <header>
        <div class="px-3 py-2 bg-dark text-white">
          <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
              <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
              </a>
    
              <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
                <li>
                  <a href="/" class="nav-link text-secondary">
                    <i class="bi bi-bank2"></i>
                    Home
                  </a>
                </li>
                <li>
                  <a href="dashboard" class="nav-link text-secondary">
                    <i class="bi bi-layout-text-sidebar-reverse"></i>
                    Dashboard
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="px-3 py-2 border-bottom mb-3">
          <div class="container d-flex flex-wrap justify-content-center">
            <form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto">
            </form>
    
            <div class="text-end">
              <button type="button" class="btn btn-light text-dark me-2"><a href ="{{url('login')}}"> Login</a></button>
              <button type="button" class="btn btn-light text-dark me-2"><a href ='{{url('register')}}'>Register</a></button>
            </div>
            <form action="/logout" method="post">
              @csrf
                <button type="submit" class="dropdown-item">Logout</button>
            </form>
          </div>
        </div>
      </header>
      <h2><marquee>BOOKSTORE GRAMMEDIA INDAH </marquee></h2>
      <center> <h6 >Jl. Imam Bonjol No.48, RT.001/RW.12, Dr. Soetomo Kec. Tegalsari, Kota Surabaya, Jawa Timur (60264)</h6></center>
     
      <div class="row justify-content-center">
        <img class="mb-3" src="../assets/logo.png" alt="" width="200" height="200" padding ="50"  >
      </div>

    <link rel="stylesheet" href="styles.css">
    <title>BOOKSTORE</title>
  </head>
  <body>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>
