<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="\css\style.css">

    <title>Hello, world!</title>
  </head>
  <body>
    <header>
        <div class="px-3 py-2 bg-dark text-white">
          <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
              <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
              </a>
    
              <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
                <li>
                  <a href="/" class="nav-link text-secondary">
                    <i class="bi bi-bank2"></i>
                    Home
                  </a>
                </li>
                <li>
                  <a href="#" class="nav-link text-secondary">
                    <i class="bi bi-layout-text-sidebar-reverse"></i>
                    Dashboard
                  </a>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="px-3 py-2 border-bottom mb-3">
          <div class="container d-flex flex-wrap justify-content-center">
            <form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto">
            </form>
    
            <div class="text-end">
              <button type="button" class="btn btn-light text-dark me-2"><a href ="{{url('login')}}"> Login</a></button>
            </div>
          </div>
        </div>
      </header>
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <main class="form-register">
            <div class="row justify-content-center">
              <img class="mb-4" src="../assets/logo.png" alt="" width="200" height="100" >
            </div>
            <h1 class="h3 mb-3 fw-normal text-center">Register Now!</h1>
          <form action="{{url('register')}}" method="post">
              @csrf
              <div class="form-floating">
                <input type="text" name ="name" class="form-control rounded-top @error ('name') is-invalid @enderror" id="name" placeholder="name" required value="{{ old('name')}}">
                <label for="name">Name</label>
                @error('name')
                  <div class="invalid-feedback">
                    {{$message}}
                  </div>
                @enderror
              </div>
              <div class="form-floating">
                <input type="text" name ="username" class="form-control @error ('username') is-invalid @enderror" id="username" placeholder="username" required value="{{ old('username')}}">
                <label for="username">Username</label>
                @error('username')
                  <div class="invalid-feedback">
                    {{$message}}
                  </div>
                @enderror
              </div>
              <div class="form-floating">
                <input type="email" name ="email" class="form-control @error ('email') is-invalid @enderror" id="email" placeholder="name@example.com" required value="{{ old('email')}}">
                <label for="email">Email address</label>
                @error('email')
                  <div class="invalid-feedback">
                    {{$message}}
                  </div>
                @enderror
              </div>

              <div class="form-floating">
                <input type="password" name ="password" class="form-control rounded-bottom" id="password" placeholder="Password" required>
                <label for="password">Password</label>
                {{-- @error('password')
                  <div class="invalid-feedback">
                    {{$message}}
                  </div>
                @enderror --}}
              </div>
              <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">Register</button>
            </form>
            <small class="d-block text-center mt-3">Already Registered?<a href="/login">Login</a></small>
        </main>
        </div>
      </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>

