<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>bookstore</title>
  </head>
  <body>
    <header>
        <div class="px-3 py-2 bg-dark text-white">
          <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
              <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
              </a>
    
              <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
                <li>
                  <a href="/" class="nav-link text-secondary">
                    <i class="bi bi-bank2"></i>
                    Home
                  </a>
                </li>
                <li>
                  <a href="dashboard" class="nav-link text-secondary">
                    <i class="bi bi-layout-text-sidebar-reverse"></i>
                    Dashboard
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="px-3 py-2 border-bottom mb-3">
          <div class="container d-flex flex-wrap justify-content-center">
            <form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto">
            </form>
            <div class="text-end">
                @auth
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      Welcome back, {{ auth()->user()->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                      <li><hr class="dropdown-divider"></li>
                      <li>
                          <form action="/logout" method="post">
                            @csrf
                              <button type="submit" class="dropdown-item">Logout</button>
                          </form>
                        </li>
                    </ul>
                  </li>
          
                @else
              <button type="button" class="btn btn-light text-dark me-2"><a href ="{{url('login')}}"> Login</a></button>
              <button type="button" class="btn btn-light text-dark me-2"><a href ='register'>Register</a></button>
              @endauth
            </div>   
          </div>
        </div>
      </header>

      <div class="container-fluid">
        <div class="row">
          <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="position-sticky pt-3">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link  {{ Request:: is ('/detail_buku') ? 'active' : '' }}" href="{{url ('/detail_buku')}}">
                    <span data-feather="file"></span>
                    DETAIL BUKU 
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ Request:: is ('/detail_pesanan_buku') ? 'active' : '' }}" href="{{url ('/detail_pesanan_buku')}}">                    <span data-feather="shopping-cart"></span>
                    PESANAN
                  </a>
                </li>
              </ul>
              
              @can('isAdmin')
              <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>BOOKSTORE</span>
                <a class="link-secondary" href="#" aria-label="Add a new report">
                  <span data-feather="plus-circle"></span>
                </a>
              </h6>
              <ul class="nav flex-column mb-2">
                <li class="nav-item">
                  <a class="nav-link {{ Request:: is ('/detail_pesanan') ? 'active' : '' }}" href="{{url ('/detail_pesanan')}}">
                    <span data-feather="file-text"></span>
                   DETAIL BUKU 
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ Request:: is ('/detail_pesanan_buku') ? 'active' : '' }}" href="{{url ('/detail_pesanan_buku')}}">
                    <span data-feather="file-text"></span>
                   Konfirmasi Pesanan 
                  </a>
                </li>
              </ul>
              @endcan
            </div>
          </nav>
      
          <main class="col-md-5 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-2 pb-1 mb-2 border-bottom">
                <img class="mb-3" src="../assets/logo.png" alt="" width="300" height="80" >
              <div class="btn-toolbar mb-3 mb-md-0">
              </div>
            </div>
          </main>
          <section id="about" class="about">
            <div class="container text-center">
              <div class="row mb-4">
                <div class="col">
                  <h1>Our Profile</h1>
                </div>
              </div>
            </div>
        
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-5 text-center">
                <p> Online shop sekarang sudah menjadi trend penggunaan yang sering di gunakan oleh banyak kalangan, trend online shop ini membuat seseorang menjadi lebih mudah untuk melakukan transaksi jual beli, karena mereka tidak harus bersusah payah untuk menghadiri tokonya, cukup memanfaatkan handphone/smartphone anda untuk melakukan transaksi jual beli lalu pesanan anda segera dikirimkan, mudah sekali bukan? kemudahan berbelanja online sebagai akibat dari perkembangan zaman dapat kita rasakan. Bookstore menjawab tantangan dari kemajuan zaman dalam bidang jual beli buku yang bersifat online.</p>
                </div>
                <div class="col-md-5 text-center">
                  <p>BOOKSTORE PAMEKASAN  adalah sebuah toko buku online yang didirikan oleh kami dengan maksud ingin memulai bisnis bersama, kami hadir sebagai salah satu toko buku online yang menjual beberapa produk buku best seller yang membantu anda untuk dapat membeli buku tanpa harus pergi ke toko buku langsung. jangan hawatir  buku yang ada di etalase kami semuanya buku best seller dan tentunya bukan barang bajakan jadi semuanya masih ori. karena kepercayaan dari konsumen adalah penting untuk kami terus melayani kebutuhan anda semua, harapan kedepan adalah menjadi ujung tombak pemasaran produk buku kami.</p>
                </div>
              </div>
            </div>
            </section>
        
        </div>
      </div>
      
      


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>