<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>BOOKSTORE</title>
  </head>
  <body>
    <header>
      <div class="px-3 py-2 bg-dark text-white">
        <div class="container">
          <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
              <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
            </a>
  
            <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
              <li>
                <a href="/" class="nav-link text-secondary">
                  <i class="bi bi-bank2"></i>
                  Home
                </a>
              </li>
              <li>
                <a href="dashboard" class="nav-link text-secondary">
                  <i class="bi bi-layout-text-sidebar-reverse"></i>
                  Dashboard
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="px-3 py-2 border-bottom mb-3">
        <div class="container d-flex flex-wrap justify-content-center">
          <form class="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto">
          </form>
          <div class="text-end">
              @auth
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Welcome back, {{ auth()->user()->name }}
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="dashboard">Dashboard</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li>
                        <form action="/logout" method="post">
                          @csrf
                            <button type="submit" class="dropdown-item">Logout</button>
                        </form>
                      </li>
                  </ul>
                </li>
        
              @else
            <button type="button" class="btn btn-light text-dark me-2"><a href ="{{url('login')}}"> Login</a></button>
            <button type="button" class="btn btn-light text-dark me-2"><a href ='register'>Register</a></button>
            @endauth
          </div>   
        </div>
      </div>
    </header>
        
    
        <main class="col-md-5 ms-sm-auto col-lg-10 px-md-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-2 pb-1 mb-2 border-bottom">
              <img class="mb-3" src="../assets/logo.png" alt="" width="300" height="80" >
            <div class="btn-toolbar mb-3 mb-md-0">
            </div>
          </div>
        </main>
      </div>
    </div>
  
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>Tanggal Rilis</th>
                <th>Judul Buku</th>
                <th>Harga Buku</th>
                <th>Stok</th>
            </tr>
           @foreach ($Pesanan as $item)
            <tr>
                
                <td>{{ $item->tanggal}}</td>
                <td>{{ $item->nama_buku}}</td>
                <td>{{ $item->stok_terjual}}</td>
                <td>{{ $item->total}}</td>
                <td>
                    {{-- <a href="{{ url('edit', $item->id)}}"><button class="btn btn-info btn-sm">edit</button> </a>
                     <a href="{{ url('hapus', $item->id)}}"><button class="btn btn-info btn-sm">hapus</button></a> --}}
                </td>
            </tr>
            @endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table>
        <div class="card-tools">
            <a href="{{url('detail_buku')}}" class="btn btn-success">Tambah<i class="fa fa_plus-square"></i></a>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

   
  </body>
</html>